VERSION := 0.0.1
BUILDOPTS := -ldflags "-X main.version=$(VERSION)"

.PHONY: build

build:
	go build $(BUILDOPTS) ./cmd/txtshare

run: build
	./txtshare
