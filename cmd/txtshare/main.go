package main

import (
	"fmt"

	"codeberg.org/whou/txtshare/server"
)

var version string
var hostname string
var port string
var publicUrl string

func main() {
	setupFlags()

	if publicUrl == "" {
		publicUrl = fmt.Sprintf("http://%s:%s", hostname, port)
	}
	handler := server.Setup(publicUrl)

	err := server.ListenAndServe(hostname, port, handler)
	if err != nil {
		panic(err)
	}
}
