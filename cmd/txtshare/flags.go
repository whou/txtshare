package main

import (
	"flag"
	"fmt"
	"os"
)

var versionFlag *bool

func setupFlags() {
	_ = flag.Bool("help", false,
		"show this help text and exit")

	versionFlag = flag.Bool("version", false,
		"show version and exit")

	flag.StringVar(&hostname, "hostname", "localhost",
		"hostname where the server will be listened")

	flag.StringVar(&port, "port", "3000",
		"port where the server will be listened")

	flag.StringVar(&publicUrl, "publicurl", "",
		"public URL of the server (default is the server HTTP hostname and port address)")

	flag.Parse()

	if *versionFlag {
		fmt.Printf("txtshare version %s\n", version)
		os.Exit(0)
	}
}
