package server

import (
	"github.com/gofrs/uuid"
)

type ShareData map[uuid.UUID]string

var database ShareData

// return share string and ID. returns empty string and a nil UUID if not found
func (shares ShareData) Search(id string) (string, uuid.UUID) {
	uid := uuid.FromStringOrNil(id)
	if uid.IsNil() {
		return "", uuid.Nil
	}

	result, found := shares[uid]
	if !found {
		uid = uuid.Nil
	}

	return result, uid
}

// delete share from data
func (shares ShareData) Remove(uid uuid.UUID) {
	delete(shares, uid)
}

// adds data to shares and return share id and error
func (shares ShareData) Add(data string) (string, error) {
	uid, err := uuid.NewV4()
	if err != nil {
		return "", err
	}

	shares[uid] = data

	return uid.String(), err
}
