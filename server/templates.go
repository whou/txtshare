package server

import (
	"html/template"
)

const TEMPLATES_PATH string = "templates/"

type ErrorTemplateData struct {
	Error   int
	Message string
}

type ShareTemplateData struct {
	Id     string
	QrCode string
}

type ReadTemplateData struct {
	Text string
}

// utility function for appending the default template path
func t(template string) string {
	return TEMPLATES_PATH + template
}

// get template page with every base template included
func GetTemplate(file string) *template.Template {
	return template.Must(template.ParseFiles(
		t("base.html"),
		t(file)))
}
