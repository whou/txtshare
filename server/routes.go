package server

import (
	"encoding/base64"
	"fmt"
	"io"
	"net/http"

	"github.com/go-chi/chi/v5"
	qrcode "github.com/skip2/go-qrcode"
)

// 404 error handler
func handler_404(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	GetTemplate("error.html").Execute(w, ErrorTemplateData{
		Error:   404,
		Message: "page not found",
	})
}

// 405 error handler
func handler_405(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	GetTemplate("error.html").Execute(w, ErrorTemplateData{
		Error:   405,
		Message: fmt.Sprintf("this page doesn't support the %s request method", r.Method),
	})
}

// "/" GET route
func route_index(w http.ResponseWriter, r *http.Request) {
	GetTemplate("index.html").Execute(w, nil)
}

// "/share" POST route
func route_share(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		GetTemplate("error.html").Execute(w, ErrorTemplateData{
			Error:   500,
			Message: "Internal Server Error: Failed to parse request body",
		})
		panic(err)
	}

	data := r.PostForm.Get("text")

	newId, err := database.Add(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		GetTemplate("error.html").Execute(w, ErrorTemplateData{
			Error:   500,
			Message: "Text share failed. Try again",
		})
		panic(err)
	}

	qrImg, _ := qrcode.Encode(fmt.Sprintf("%s/read/%s", publicServerUrl, newId), qrcode.Low, 256)
	qrData := base64.StdEncoding.EncodeToString(qrImg)

	GetTemplate("share.html").Execute(w, ShareTemplateData{
		Id:     newId,
		QrCode: qrData,
	})
}

// "/read/{id}" GET route
func route_read(w http.ResponseWriter, r *http.Request) {
	idParam := chi.URLParam(r, "id")

	share, id := database.Search(idParam)
	if id.IsNil() {
		w.WriteHeader(http.StatusNotFound)
		GetTemplate("error.html").Execute(w, ErrorTemplateData{
			Error:   404,
			Message: "Share not found",
		})
		return
	}

	database.Remove(id)
	GetTemplate("read.html").Execute(w, ReadTemplateData{
		Text: share,
	})
}

// "/txt" GET route
func route_txt_index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(
		"Welcome to txtshare!\n" +
			"Send a POST request with text to /txt/share and keep the ID.\n" +
			"Now read it somewhere else by accessing /txt/read/{ID}!"))
}

// "/txt/share" POST route
func route_txt_share(w http.ResponseWriter, r *http.Request) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Internal Server Error:\nFailed to read request body"))
		panic(err)
	}

	newId, err := database.Add(string(data))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to share text :(\nTry again."))
		panic(err)
	}

	w.Write([]byte(newId))
}

// "/txt/read/{id}" GET route
func route_txt_read(w http.ResponseWriter, r *http.Request) {
	idParam := chi.URLParam(r, "id")

	share, id := database.Search(idParam)
	if id.IsNil() {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Share not found :("))
		return
	}

	database.Remove(id)
	w.Write([]byte(share))
}
