package server

import (
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

var publicServerUrl string

// Sets up a new chi handler and return it
func Setup(publicUrl string) *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.NotFound(handler_404)
	r.MethodNotAllowed(handler_405)

	r.Get("/", route_index)
	r.Post("/share", route_share)
	r.Get("/read/{id}", route_read)

	r.Route("/txt", func(r chi.Router) {
		r.Get("/", route_txt_index)
		r.Post("/share", route_txt_share)
		r.Get("/read/{id}", route_txt_read)
	})

	database = make(ShareData)
	publicServerUrl = publicUrl

	return r
}

func ListenAndServe(hostname string, port string, handler *chi.Mux) error {
	if hostname == "" {
		hostname = "localhost"
	}

	address := fmt.Sprintf("%s:%s", hostname, port)

	listen, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}

	log.Printf("Listening on http://%s", address)
	log.Printf("The server public URL is %s", publicServerUrl)

	return http.Serve(listen, handler)
}
