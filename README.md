# txtshare
A stupid server to share text across multiple devices.

## How to use
1. Send a `POST` request to `/share` with the shared text on request body.
2. Copy the returned ID.
3. Access `/read/{ID}`, replacing `{ID}` with the previously copied ID.

## Build
- Go >= 1.19
- GNU Make

```
make
```

Now run `./txtshare` :)

# License
txtshare is licensed under [AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.html) or later. You may freely copy, distribute and modify it. Any modifications, including on network use, must also be distributed under AGPL. You can read the [COPYING](./COPYING) file for more information.