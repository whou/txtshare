module codeberg.org/whou/txtshare

go 1.19

require github.com/go-chi/chi/v5 v5.0.10

require github.com/gofrs/uuid v4.4.0+incompatible

require github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
